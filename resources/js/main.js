$(function() {
  
    var max = 20;
    var imageArray = ["http://lorempixel.com/600/600/abstract/", "http://lorempixel.com/600/600/sports/", "http://lorempixel.com/600/600/food/", "http://lorempixel.com/600/600/city/", "http://lorempixel.com/600/600/business/", "http://lorempixel.com/600/600/people/", "http://lorempixel.com/600/600/nature/", "http://lorempixel.com/600/600/technics/", "http://lorempixel.com/600/600/fashion/", "http://lorempixel.com/400/200/cats/",
    "http://lorempixel.com/600/600/abstract/", "http://lorempixel.com/600/600/sports/", "http://lorempixel.com/600/600/food/", "http://lorempixel.com/600/600/city/", "http://lorempixel.com/600/600/business/", "http://lorempixel.com/600/600/people/", "http://lorempixel.com/600/600/nature/", "http://lorempixel.com/600/600/technics/", "http://lorempixel.com/600/600/fashion/", "http://lorempixel.com/400/200/cats/"];
    
    $( "#slider" ).slider({
      value:0,
      min: 0,
      max: max,
      step: 1,
      slide: function( event, ui ) {
        var img = imageArray[ui.value];
        $("#images-container").css("background-image", "url(" + img + ")");
      }
    });
  
    for (i = 0; i < (max+1); i++) {
    	var spacer = $("#slider").width() / max;
	    $( "#slider-wrap" ).append("<div style='left:" + (spacer * i) + "px;' class='slider-ball-wrap'><small>" + (i/10) + "</small><div class='slider-ball'></div></div>");
	}
  
  });