var gulp = require('gulp'), 
    sass = require('gulp-ruby-sass') 
    notify = require("gulp-notify") 
    bower = require('gulp-bower')
    concat = require('gulp-concat');

var config = {
     sassPath: './resources/sass',
    jsPath: './resources/js',
     bowerDir: './bower_components' 
}

gulp.task('bower', function() { 
    return bower()
         .pipe(gulp.dest(config.bowerDir)) 
});

gulp.task('icons', function() { 
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*') 
        .pipe(gulp.dest('./public/fonts')); 
});

gulp.task('js', function() { 
    return gulp.src([config.bowerDir + '/jquery/dist/jquery.js', config.bowerDir + '/jquery-ui/ui/jquery-ui.js', './resources/js/main.js'])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./public/js/'));
});

gulp.task('cssui', function() { 
    return gulp.src([ config.bowerDir + '/jquery-ui/themes/base/jquery-ui.css'])
    .pipe(concat('all.css'))
    .pipe(gulp.dest('./public/css/'));
});

gulp.task('css', function() { 
    return gulp.src(config.sassPath + '/style.scss')
         .pipe(sass({
             style: 'compressed',
             loadPath: [
                 './resources/sass',
                 config.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
                 config.bowerDir + '/fontawesome/scss',
             ]
         }) 
            .on("error", notify.onError(function (error) {
                 return "Error: " + error.message;
             }))) 
         .pipe(gulp.dest('./public/css')); 
});

// Rerun the task when a file changes
 gulp.task('watch', function() {
     gulp.watch(config.sassPath + '/**/*.scss', ['css','cssui']); 
});

  gulp.task('default', ['bower', 'icons', 'css', 'cssui', 'js']);
